#Endometrial Cancer Project Notes and Ideas: Shelley & Steve


Note from: Shelley Marie Macneil <s.macneil@utah.edu>
	
Apr 29
		
to Steve
I found some more Ovarian Samples, 93 from an Australian data set, but all of of samples appear to be of serous subtype using the HiScan microarray platform(HumanOmni2.5-8 BeadChip Kit). Not sure if that is helpful, because they are not Affymetrics or Aglient. Maybe we could add this data set as more serous samples to our PANCAN GSOA run so we will have another data set from a different source in there.
http://dcc.icgc.org/projects/OV-AU

I found some more serous ovarian samples on GO. There a couple other ovarian studies on there with sample numbers ranging from 10-40. But we don't need more serous samples, or do we need just non-serous ovarian to compare to?
<http://www.ncbi.nlm.nih.gov/sites/GDSbrowser?acc=GDS3297>

The other breast samples on ICGC are from the UK, but the files just state that they are triple negative, they do not specify basal-like or not. So, these are probably not useful. I found an all-basal tumor study with 47 samples on GO. http://www.ncbi.nlm.nih.gov/sites/GDSbrowser?acc=GDS2250

Also, we would not want any samples that are cells lines I assume? Just tumors? Because there is a lot of cell line data. 

