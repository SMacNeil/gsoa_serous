GSOA: Serous Cancer Project Methods and Results    
------------------------------------------------------------------------------------------
       
       
## PANCAN File Generation for GSOA Analysis

The 12 PANCAN12 files were generated contain a 'TCGA ID' and whether the the cancer is "Serous or Non-Serous"

###1) Endometrial and Breast Cancer Data was downloaded and files were created from TCGA Dataportal

Samples Downloaded From: *Integrated genomic characterization of endometrial cancer, Nature 2013* <https://tcga-data.nci.nih.gov/docs/publications/ucec_2013/>   

**Endometrial Cancer (UCEC) (53 Serous and 307 Non-Serous Samples)**     

Original File: datafile.S1.1.KeyClinicalData.tsv   
Original Amt. of Samples: 373      
Method: Changed the file extension to .txt, opened in excel, resaved as 'TCGA_ClincalData_Nature2013_EndometrialCancerPaper_Serous.txt', deleted unwanted data, and parsed in Linux. This was done because the file could not be parsed properly.   

```
cat /Users/Shasta/Documents/TCGA_ClincalData_Nature2013_EndometrialCancerPaper_Serous.txt 
| grep "Serous" > TCGA_UCEC_Nature2013_360.txt
```
Note: Samples were removed that were Mixed Serous/Non_Serous

**Basal-like Breast Cancer (98 Serous(Basal) and 447 Non-Serous Samples)**
Original File: 'TCGA_BreastCancerSubtypes.txt' (obtained from Steve Piccolo)    
Original Amt. of Samples: 547   
Method: Original file was parsed in Linux  
Note: Samples were removed that were Mixed Serous/NonSerous
```    
cat /Users/Shasta/Desktop/PanCan/TCGA_BreastCancerSubtypes.txt | awk '{gsub(/LumA|LumB|Her2|Normal/, "Non-Serous")}1 '| awk '{gsub(/Basal/, "Serous")}1 ' | awk '{gsub(/-01A|-01B|-06A|-11A|-11B/,"")}1' > TCGA_BreastCancer_Serous_Non-Serous_547.txt
```

###2) The other 10 cancer types were downloaded and files were created from Synapse.org
 
All Files Downloaded From: <https://www.synapse.org/Portal.html#!Synapse:syn300013/wiki/>
Files: A (.txt) extension was added to view to excel before parsing, but file was un-manipulated in excel and original file was parsed in Linux at the command line

**Endometrial Cancer (UCEC) from PANCAN-12 (433 Serous and Non-Serous Samples)**
Original File: 'tcga_UCEC_clinical.patient'      
```
cat /Users/Shasta/Desktop/PanCan/Orginal_TCGA_ClinicalData/tcga_UCEC_clinical.patient | awk -F "\t" '{print$1"\t"$25}' | sed 's/Endometrioid endometrial adenocarcinoma/Non-Serous/' | sed 's/Serous endometrial adenocarcinoma/Serous/' | sed 's/Non-Serous (Grade 1)/Non-Serous/' | sed 's/Non-Serous (Grade 2)/Non-Serous/' | sed 's/Non-Serous (Grade 3)/Non-Serous/' | sed 's/Non-Serous (Grade 1 or 2)/Non-Serous/' | grep "Serous" > TCGA_UCEC_Serous_Non-Serous_433.txt  
```

**Ovarian Cancer from PANCAN-12 (571 Serous ONLY Samples)**
Original File: 'tcga_OV_clinical.patient' (Whitelist has 6 less than the .patient file)    
Original Amt. of Samples: 600      
Note: Samples were removed that had no information about if Serous or Not
```
cat /Users/Shasta/Desktop/PanCan/Orginal_TCGA_ClinicalData/tcga_OV_clinical.patient | awk -F "\t" '{print$1"\t"$48}' | sed  s'/Serous Cystadenocarcinoma/Serous/' | grep "Serous" > TCGA_OV_SerousOnly.txt  
```
   
**Rectum adenocarcinoma (READ) (169 Non-Serous Samples)**
Original File:tcga_READ_clinical.patient.txt     
```
cat /Users/Shasta/Desktop/PanCan/Orginal_TCGA_ClinicalData/NotSerous/tcga_READ_clinical.patient.txt 
|awk -F "\t" '{print$1"\t" "Non-Serous"}' | grep "TCGA" > TCGA_READ_NonSerous_169.txt
```

**Lung squamous cell carcinoma (LUSC) (389 Non-Serous Samples)**
Original File: tcga_LUSC_clinical.patient.txt     
``` 
cat /Users/Shasta/Desktop/PanCan/Orginal_TCGA_ClinicalData/NotSerous/tcga_LUSC_clinical.patient.txt 
|awk -F "\t" '{print$1"\t" "Non-Serous"}' | grep "TCGA" > TCGA_LUSC_NonSerous_389.txt
```

**Glioblastoma multiforme (GMB) (599 Non-Serous Samples)**
Original File: tcga_GBM_clinical.patient.txt      
```
cat /Users/Shasta/Desktop/PanCan/Orginal_TCGA_ClinicalData/NotSerous/tcga_GBM_clinical.patient.txt 
|awk -F "\t" '{print$1"\t" "Non-Serous"}' > TCGA_GBM_Nonserous_599.txt
```

**Bladder Urothelial Carcinoma (BLCA) (153 Non-Serous Samples)** 
Original File: tcga_BLCA_clinical.patient.txt     
```
cat /Users/Shasta/Desktop/PanCan/Orginal_TCGA_ClinicalData/NotSerous/tcga_BLCA_clinical.patient.txt 
|awk -F "\t" '{print$1"\t" "Non-Serous"}' | grep "TCGA" > TCGA_BLCA_NonSerous_153.txt
```

**Colon adenocarcinoma (COAD) (423 Non-Serous Samples)**
Original File: tcga_COAD_clinical.patient.txt      
```
cat /Users/Shasta/Desktop/PanCan/Orginal_TCGA_ClinicalData/NotSerous/tcga_COAD_clinical.patient.txt 
|awk -F "\t" '{print$1"\t" "Non-Serous"}' | grep "TCGA"> TCGA_COAD_NonSerous_423.txt
```

**Acute Myeloid Leukemia (LAML) (202 Non-Serous Samples)**
Original File: tcga_LAML_clinical.patient.txt        
```
cat /Users/Shasta/Desktop/PanCan/Orginal_TCGA_ClinicalData/NotSerous/tcga_LAML_clinical.patient.txt 
| awk -F "\t" '{print$1"\t" "Non-Serous"}' | grep "TCGA" > TCGA_LAML_NonSerous_202.txt
```

**Head and Neck squamous cell carcinoma (HNSC) (343 Non-Serous Samples)**
Original File: tcga_HNSC_clinical.patient.txt      
```
cat /Users/Shasta/Desktop/PanCan/Orginal_TCGA_ClinicalData/NotSerous/tcga_HNSC_clinical.patient.txt 
| awk -F "\t" '{print$1"\t" "Non-Serous"}' | grep "TCGA" > TCGA_HNSC_NonSerous_343.txt
```

**Lung adenocarcinoma(LUAD) (542 Non-Serous Samples)**
Original File: tcga_LUAD_clinical.patient.txt     
```
cat /Users/Shasta/Desktop/PanCan/Orginal_TCGA_ClinicalData/NotSerous/tcga_LUAD_clinical.patient.txt 
| awk -F "\t" '{print$1"\t" "Non-Serous"}' | grep "TCGA" > TCGA_LUAD_NonSerous_542.txt
```

**Kidney renal clear cell carcinoma(KIRC) (502 Non-Serous Samples)**
Original File: tcga_KIRC_clinical.patient.txt     
```
cat /Users/Shasta/Desktop/PanCan/Orginal_TCGA_ClinicalData/NotSerous/tcga_KIRC_clinical.patient.txt 
| awk -F "\t" '{print$1"\t" "Non-Serous"}' | grep "TCGA" > TCGA_KIRC_NonSerous_502.txt
```

###3) All 12 PANCAN files were merged into one file  (PanCan12MergedFile.txt)    

```
cat TCGA*.txt > PanCan12MergedFile.txt
```
Results:     
4899 Total Samples: 4077 Non-Serous ; 722 Serous ; 15% of the samples are Serous

#added all data to an excel document

##GSOA analysis on endometrial samples only

changed serous file name vim to "endometrail_serous" for analysis

````nohup ./serous &
    tail -f nohup.out````


##Connecting and using the server    

1) Connect to *gw2.genetics.utah.edu*     
2) Connect to the ~ticvah server and enter password   
```ssh shelleym@tikvah```   
3) Copying a file to a from the local computer to the server   
from the local computer to the server   
```mycomputer$ scp PanCan12MergedFile.txt shelleym@tikvah:/data2/Vagrant/gsoa_manuscript/gsoa/Data/PANCAN12_Serous.txt```    
from the sever to the local computer (whole directory)   
````mycomputer$ scp -r shelleym@tikvah:/data2/Vagrant/gsoa_serous/gsoa/Results/ . ```


##Running the GSOA Analysis 
1) edit serous file in vim
2)Run the analysis
```shelleym@tikvah gsoa_serous]$ nohup ./serous &```

*GSOA analysis on endometrial samples only*
changed serous file name vim to "endometrail_serous" for analysis

````nohup ./serous &
    tail -f nohup.out``
  

## Setting up and using GitBucket
See 'git help COMMAND' for more information on a specific command.

1) Create a under name and password on <https://bitbucket.org>
2) Create a repository 
3) Make a directory on your machine that has the same name as Bitbucket repository
````[shelleym@tikvah Vagrant]$ mkdir gsoa_serous````
4) Initialize the git repository
````
[shelleym@tikvah gsoa_serous]$ git initInitialized empty Git repository in /data2/Vagrant/gsoa_serous/.git/
[shelleym@tikvah gsoa_serous]$ git remote add origin git@bitbucket.org:SMacNeil/gsoa_serous.git
````
5) Adding Steves gsoa repository to mine 
````
[shelleym@tikvah gsoa_serous]$ git clone git@bitbucket.org:srp33/gsoa.git
Initialized empty Git repository in /data2/Vagrant/gsoa_serous/gsoa/.git/
````
6) Committing results to Git Bucket  
```[shelleym@tikvah gsoa_serous]$ ./commit_git```  

7) Copy results from Results to gsoa_serous repository
````cp gsoa/Results/* Results````

8) Make a .gitignore file and tell it which directories to ignore
```
vim .gitignore
cat .gitignore 
gsoa
```

##Write Python Program to Parse Files for Heatmap

1) wrote * ParsePathwayData.py*
Input: gmt, RNA-Seq, and Class file
Output: Only RNA samples we want

##Creating Heatmaps

1) obtained *PlotHeatmap.R* from Steve
2)Made a bash script to call *PlotHeatmap.R* and *ParsePathwayData.py* > *create_heatmaps*
2) Made heat map for pathways with a high p-value
3) Made the heat maps better
4) Averaged non-serous and serous samples 
5) Z-score did not work out well. (z score allows you to look at all genes)
- do the z score first and then average them all together 
- apply z score for each coloumn ( genes values)
- pull out S and NS with unique function, store are variables
- use stop() to end code 
- rbind - merges two matrixces 
- think about how we want it to look
- do the t test 
- show only the genes that have a sig. p value  for RNA seq and pull them out for all datatypes 
- evenutally plot the values (x y)
- figure out statistics 
	-t test might not be okay 
	- look for an R package the does FDR
	- find t test alterinatives 



##Calculating the Ranked Based P Value for Endometrial and PANCAN(31March14)
1) Copy over 'CalculateRankPvalue.R' which useds the WCGNA package 
2) Put into shell script 'summarizeresults' that calls 'CalculateRankPvalue.R'. 
3)Ran for Endometrial and Serous
````./summarizeresults````
- PANCAN12_Endometrial_Serous_tumor_5_svmrbf_RankPValue.txt
- PANCAN12_Endometrial_Serous_tumor_5_svmrbf_RankPValue.txt

                                                       
#Notes for looking at PANCAN and Endometrial by hand in Excel for unranked p values
- Finding important pathways to plot)
- Focused on P-values
- Pathways that difference between serous and nonserous
*Pancan*
1) None common across all
2) 20 common in RNA and Somatic
3) 20 common in Somatic and CNV
4) 2 somatic and methylation
*Endometrial*
- Erbb2 in meth and somatic
Wonder why you would see a difference in methylation and somatic? Was it mutated then suppressed?
Better to look at ones that arn't somatic???



##Quality control - Negative Control 
Permuated anaylsis
-do you see that certain pathways come up all the time, or do they change. 
- run 1000 permuations and see if any pathways just randomly come up? 
- aucs should be around 5 
- it is not just random 

Method:
-randomly permuate the labels 
-neg. control 
-python or R
- randomly shuffle 
- not a trend for cancer pathways at the top 
-gsea used a similar approach 

##Spotchecking to make sure our parsed RNA file is correct (WNT_SIGNALLING)
- loaded the head -1000 and tail -1000 of the total RNA seq data for pan can into excel
- when through and made sure everything was watching up    
- The tail end is good. Checked 4 genes first 10-15 in the front and back and one in the middle

cat KEGG_PATHWAYS_IN_CANCER | awk -F'\t' '{print NF;exit}'
2953

----------------------------------------------------------------------------------
                   
##looking at Normals in PANCAN

Synapse user name: s.macneil@utah.edu
pw: the usual


Can we compare normal to tumor figure out the back ground? Filtering???
 
Should we compare all of the Serous to the Normals to see what driving it?

Should we just compare Normals to Endometrial Serous we can see is there are somatic mutations driving it?

How many do we have? 
-just just the patient names and count
 awk -F'\t' '{print NF;exit}'
 
 *PANCAN*
 
-RNA-seq (11%)
	-2702(of 3273) Tumor 	(PANCAN12.IlluminaHiSeq_RNASeqV2.geneExp.tumor_whitelist.txt)
	- 302 Normals   	(PANCAN12.IlluminaHiSeq_RNASeqV2.geneExp.normal_whitelist.txt)

-SNPs, or CNV, Copy Number Variation (matched)
	-  4796 Normals
	-  4772 Tumor
	
- Methylation (15%)
	- 2225 Tumors		(PANCAN12.HumanMethylation27.DNAMethylation.tumor_whitelist.txt
	- 333 Normals 	(PANCAN12.HumanMethylation27.DNAMethylation.normal_whitelist.txt)
	
	
- MAFs (somatic mutations) 100% I believe
    - 3497 Normal (Pancan12.cleaned.Mafs)
	- 2397 Tumor  (Pancan12.cleaned.Mafs)
    - Used | sort -u

	
*Keep in mind that not all of our serous/non-serous samples always show up here, for example,
there were 3273 RNA-seq files total, but only 2702 of our 4700 serous samples matched these. 

How many serous do we have Total? 722 serous
How many Non-serous? 4077 Non-Serous
	
Could be reasonable to compare Serous to Normals in somatic mutations and SNPs, and methylation. 
	-There are not many for RNA-seq though, so this might not be meaningful because you won't be able to see what gene expression is going on. 
	- Could we compare the 302 Normal to 722 Serous?

- We have endometrial data from the paper and from Synpase. 

From synapse(We have 433 Serous and Non-Serous Files)
-SNPs, CNVs
	- 505 Normals
	- 494 Tumors
	- Why have more normals than tumors?
	
-Methylation (11%)
	- 43 Normal
	- 384 Tumor

-MAF 
	-249 Matched Normals and Tumors
	- Used | sort -u

- RNA-Seq
	- 6 Normal
	- 334 Tumor
	
From paper (We have 374 Serous and Non-Serous Files)
	- 53 serous I think
	- 

Endomtrail: We have good matched for CNVS and  MAFS(somatic), so we can ask:
	1) What somatic mutations occured that lead to serous endometrial cancer?
	2) Are there chromosomal abnormalities associated?


##Types of files from PanCan

-tcga_READ_clinical.patient (medical history about each patient and cancer type)


##Summarize_results - correlating all the -omic types (16April2014)


1) Make a bash script that calls PlotGeneSetaggrements.R, and takes all the parameters (2 files to be compared, outfile (pdf), and x and y axes)
2) View on my computer with scp
3) Analysis results 
	- generates an 'r' correlation co-efficient that measures the strength of the linear relationship between Two Variables
	- R^2 tells you how different they are, regression analysis, 'coefficent of determination'.

PanCAN
CNV vs Methylation *(r = 0.619  )*    
RNA-seq vs. CNV *(r =0.82  )* 
RNA-seq vs SomaticMutations (MAF) *(r = 0.404 )* 
RNA-seq vs Methylation *(r = 0.594  )* 
SomaticMuatations vs CNV *(r =-.454  )* 
SomaticMuatations vs Methylation *(r = 0. 391 )* 

Endometrial 
CNV vs Methylation *(r = 0.283 )*    
RNA-seq vs. CNV *(r =0.82  )* 
RNA-seq vs SomaticMutations (MAF) *(r = 0.404 )* 
RNA-seq vs Methylation *(r = 0.594  )* 
SomaticMuatations vs CNV *(r =-.454  )* 
SomaticMuatations vs Methylation *(r = 0. 391 )* 

/data2/Vagrant/gsoa_serous/gsoa/Results/PANCAN12_Endometrial_Serous_CNV_tumor_5_svmrbf_AUC.txt
/data2/Vagrant/gsoa_serous/gsoa/Results/PANCAN12_Endometrial_Serous_Methylation_tumor_5_svmrbf_AUC.txt
/data2/Vagrant/gsoa_serous/gsoa/Results/PANCAN12_Endometrial_Serous_Mutations_tumor_5_svmrbf_AUC.txt
/data2/Vagrant/gsoa_serous/gsoa/Results/PANCAN12_Endometrial_Serous_RnaSeq_tumor_5_svmrbf_AUC.txt 
4) Lay them all out together


## Using DESeq2 to analyze RNA-seq data. 
*Measure gene expression differences using RNAseq Data*

1) Install the DESeq2 Package 
 *Won't work because our data is already analyzed with the TCGA normalized pipeline, try Limma*
 
## Using Limma to analyze RNA-seq Data
- Started off with 360 Endo samples, but only 323 were in the RNA-seq file.	
- Limma is a package on bioconducter 
- Uses an empirical Bayes method to moderate the standard errors of the estimated log-fold changes
- Data has already beed normalized with RSEM and quartile 

1) install limma
	```` source("http://bioconductor.org/biocLite.R")
		 biocLite("limma")
		 citation("limma")
		vignette("limma")
		help(package=limma)
        library(limma) ````

2)Making the design matrix (Which RNA samples have been applied to each array)
	- Each row of the design matrix corresponds to an array in your experiment(sample) and each column corresponds
	 		to a coefficient that is used to describe the RNA sources in your experiment( serous or nonserous)
	- Need to turn our class file (Patient ID x Serous or Non-Serous) into a design matrix
```` Group <- factor(classData$V2, levels=c("Serous","Non-Serous"))
	design <- model.matrix(~0+Group)
	colnames(design) <- c("Serous","Non-Serous") ````
	-Both of class file and RNA-seq file are in the same order as patient Ids
	
3) Contrast Matrix	(which comparisons you would like to make between the RNA samples)
````contrast.matrix <- makeContrasts(Serous-NonSerous, levels=design)````

4) Voom Transform 
- Transforms count data to log2-counts per million, estimate the mean-variance relationship and 
use this to compute appropriate observational-level weights. The data are then ready for linear modelling. 
````v <- voom(data,design,plot=TRUE)
	fit <- lmFit(v, design)
	fit <- eBayes(fit)
	topTable(fit,coef=ncol(design)) #just shows summary
	````
*Not sure if we need to do anything, but it looks like we can put RSEM data into voom

5) Results
- Viewed with topTable and saved to a text file. 
- Cannot find the adj. P value in the text file
- Why are they all significant even in the neg control. 

1) Patient IDs are not showing up on the design matrix (not sure if that matters)
2) All the genes are significant, even the neg control

1) Make sure the the RNA-seq values going into Limma are the same as the Original RNA-seq file from TCGA

- Pick a pathway (I did P53_Downstream and Taste), both of the RNA-seq files generated from the ParseHeatMap.R program 
matched up with the original RNA seq file, so no problem there. 

ParsePathwayData.R:  Takes a .gmt file parses out the wanted genes, and takes an RNA seq file and parses out whe wanted patients
by reading the class file. 

The out of of this program is correct. (correct genes and correct RNA values)

Now we got to the Heatmap file. Takes the file that was generated by Parse....R (which is correct), and takes that file into a matrix
and also turns the class file into a matrix (patient samples match up with both these), and does a differental expression on them. 


2) Plot a histogram for 3 genes at the top and 3 at the bottom for a postive and negative control to see if they are really different. 

Histogram indicates the number of data points that lie within a range of vaules. Plot a histogram of the serous vs nonserous.





Notes from : Integrated genomic characterization of endometrial cancinoma (TCGA)


Okay 




          





