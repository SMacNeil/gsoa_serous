suppressPackageStartupMessages(require(gplots))
suppressPackageStartupMessages(library(RColorBrewer, quiet=TRUE))

inFilePath = commandArgs()[7]
classFilePath = commandArgs()[8]
outFilePath = commandArgs()[9]
OutFiletxt = commandArgs()[10]

data = log2(data.matrix(read.table(inFilePath, sep="\t", header=TRUE, stringsAsFactors=FALSE, row.names=1, quote="\"", check.names=FALSE)))
#dim(data) # 23 x 323, there was only RNA-seq values for 323 of the 360 samples
#head(data)

classData = as.matrix(read.table(classFilePath, sep="\t", header=FALSE, row.names=1, stringsAsFactors=FALSE))
#classData = classData[classData[,1] %in% colnames(data),]
classData = classData[colnames(data), , drop=FALSE] # make sure classData rows are in same order as expression columns
dim(classData) # 1 x 323

#Seperates the Class Samples into Seperate Variables
class1 = sort(unique(classData[,1]))[1]
class2 = sort(unique(classData[,1]))[2]
class1Samples = rownames(classData[which(classData[,1]==class1), , drop=FALSE])
class2Samples = rownames(classData[which(classData[,1]==class2), , drop=FALSE])

data1 = data[,class1Samples]
data2 = data[,class2Samples]

#Tests
#DUSP3_NS = (data1[1,]) #all the NS gene counts for each sample
#DUSP3_S = (data2[1,])
#pValue = t.test(DUSP3_NS, DUSP3_S)
#print(pValue)
#wilcox= wilcox.test(DUSP3_NS, DUSP3_S)
#wilcox

geneWilcoxPValues = NULL 
for (i in 1:nrow(data))
{
  pValue = wilcox.test(data1[i,], data2[i,])$p.value
  print(pValue)
  geneWilcoxPValues = c(geneWilcoxPValues,pValue)
}

geneWilcoxPValues
#typeof(pValue) 

outData = cbind(rownames(data), geneWilcoxPValues)
write.table(outData, OutFiletxt, sep = "\t", quote=FALSE, row.names=FALSE, col.names=FALSE)

data1 = data1[which(geneWilcoxPValues<=0.05),]
data2 = data2[which(geneWilcoxPValues<=0.05),]

#Averages all of the P-values for Serous vs. NonSerous
data1Means = apply(data1, 1, mean)
data2Means = apply(data2, 1, mean)

#Brings together the two means into one variabe?
dataMeans = rbind(data1Means, data2Means)
dataMeans

#Assigns Serous or Non Serous to the Columns
rownames(dataMeans) = c(class1, class2)

colors = brewer.pal(11,"RdBu")
key = TRUE
cexRow = 1
cexCol= 1.5
pdf(outFilePath, width=5, height=10)
heatmap.2(t(dataMeans), Rowv=TRUE, col=colors, dendrogram="row", trace="none", key=TRUE,  symkey=FALSE,, density.info="none", scale="none", main="", cexRow=cexRow,cexCol=cexCol,  labRow=colnames(dataMeans), labCol=rownames(dataMeans),margin=c(6,16), lmat=rbind(c(4,3),c(2,1)), lwid=c(2,4), lhei=c(2.0,14))

#margin=FALSE, lmat=rbind(c(0,3),c(2,1),c(0,4)), lwid=c(1.5,4), lhei=c(1.5,4,1),srtCol=180)

graphics.off()
