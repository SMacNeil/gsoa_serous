import sys,string

dataFile = sys.argv[1]
datasetName= sys.argv[2]
pathwayFile=sys.argv[3]
fileName=sys.argv[4]
outFile=sys.argv[5]

results_dict={}
data= open(dataFile, 'r')
for line in data:

    lineItems=line.rstrip().split("\t")
    pathway=lineItems[0]
    pvalue=lineItems[1]
    results_dict[pathway]= pvalue

dataset_dict={}
pathwayInfo = open(pathwayFile, 'r')
for line in pathwayInfo:
   lineItems = line.rstrip().split()
   GEOset=lineItems[0]
   targetPathway=lineItems[1]
   dataset_dict[GEOset]= targetPathway

KEGGpathway=dataset_dict[datasetName]
Pvalue=results_dict[KEGGpathway]

print KEGGpathway
print Pvalue

outfile= open(outFile, "a")
outfile.write('%s\t%s\t%s\n' % ( fileName, KEGGpathway, Pvalue))


