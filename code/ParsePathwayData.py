import sys
#This progrom parses out the specifed genes from the desired pathway as well as the desired patient IDS from the RNA seq file.
#Returns a matrix where genes are the rows, patient IDS are the coulumns, and RNA-seq count data are the values.

queryPathwayName = sys.argv[1]

#load in the gmt file into a dictionary
gmtFile = typeFile = open(sys.argv[2])
pathwayGenes = set()
for line in gmtFile:
    lineItems = line.rstrip().split("\t")
    PathwayName = lineItems[0]
    Genes = set(lineItems[2:])

    if PathwayName == queryPathwayName:
        pathwayGenes = Genes

#load in the serous/non-serous file
type_file = open(sys.argv[3])
type_dict = {}
for line in type_file:
    line_items = line.rstrip().split("\t")
    sample_id = line_items[0]
    cancer_type = line_items[1]
    type_dict[sample_id] = cancer_type  #this one is working

#firstKey = sorted(type_dict.keys())[0]
#print type_dict[firstKey]

#load in the RNA-seq Data
rna_file = open(sys.argv[4],"rU")
rna_file_header_items = rna_file.readline().rstrip().split("\t")
rna_sample_ids = rna_file_header_items[1:]
analysis_sample_ids = set(rna_sample_ids) & set(type_dict.keys())
analysis_sample_ids_out = sorted(list(analysis_sample_ids))

line_count = 0
heatmapData = []
heatmapData.append([""] + analysis_sample_ids_out)

for line in rna_file:
    line_count += 1
    if line_count % 1000 == 0:
        print line_count

    lineItems = line.rstrip().split("\t")
    gene = lineItems[0]
    values = lineItems[1:]

    if gene not in pathwayGenes:
        continue

    valuesDict = {}
    for i in range(len(rna_sample_ids)):
        sample_id = rna_sample_ids[i]
        valuesDict[sample_id] = values[i]

    outValues = [valuesDict[sample_id] for sample_id in analysis_sample_ids_out]
    lineOutItems = [gene] + outValues
    heatmapData.append(lineOutItems)

outFile = open(sys.argv[5] + "/" + queryPathwayName, 'w')

for row in heatmapData:
    outFile.write("\t".join(row) + "\n")

outFile.close()

#k
	#rna_dict = dict.fromkeys(patient_id_2, [])
    #else:
    #    data = line.strip().split("\t")
    #    gene_name= data[0]
    #    for i,val in enumerate(data[1:]):
    #           rna_dict[patient_id_2[i]].append((gene_name, val))
    #           #print rna_dict
    #          # raw_input('continue')


#if PathwayName in gmtDict:
#    geneList = gmtDict[PathwayName]
#    print PathwayName
#    print geneList
#
#patient_id_3 = {}
  # for i in rna_dict:
	#if patient_id in rna_dict:
	    # patient_id_3 =



rna_file.close()
type_file.close()
gmtFile.close()
